package main

import (
	"io"
	"os"
	"sync"
	"testing"
)

func TestPrintConcurrently(t *testing.T) {
	stdout := os.Stdout

	r, w, _ := os.Pipe()
	defer w.Close()

	os.Stdout = w
	var wg sync.WaitGroup
	strs := []string{"John", "Lion", "people"}

	wg.Add(len(strs))
	for _, x := range strs {
		go myPrintFunc(x, &wg)
	}

	wg.Wait()

	result, _ := io.ReadAll(r)
	output := string(result)

	os.Stdout = stdout
	t.Log(output)
}