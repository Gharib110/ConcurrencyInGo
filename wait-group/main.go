package main

import (
	"fmt"
	"sync"
)

func myPrintFunc(msg string, wg *sync.WaitGroup)  {
	defer wg.Done()
	fmt.Println(msg)
}

func main() {
	messages := []string{"Pi", "Zi", "Di", "Hi"}

	var wg sync.WaitGroup

	wg.Add(len(messages))

	for _, msg := range messages {
		go myPrintFunc(msg, &wg)
	}

	wg.Wait()

	fmt.Println("End of FILE ....")

	return
}