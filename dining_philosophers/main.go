package main

import "sync"

var phils = []string{"Plato", "socrates", "Kafka", "Abo", "Marshal"}

var wg sync.WaitGroup

func diningPhilosophers(philosopher string, forkLeft, forkRight *sync.Mutex) {
	wg.Done()
}

func main() {

	forkLeft := &sync.Mutex{}
	wg.Add(len(phils))
	for i := 0; i < len(phils); i++ {
		forkRight := &sync.Mutex{}
		go diningPhilosophers(phils[i], forkLeft, forkRight)

		forkLeft = forkRight
	}

	wg.Wait()
}
